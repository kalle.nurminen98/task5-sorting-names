# Sorting names with iteration algorithm

Reads file and puts names to empty vector. After that vector is looped through in iteration algorithm. Then the program prints the first 1000 names sorted.

## Requirements

requires gcc

## Usage

$ cd src

$ g++ main.cpp -o main

$ ./main

## Maintainers

Kalle Nurminen 