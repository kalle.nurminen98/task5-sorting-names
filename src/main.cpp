#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>

int main()
{
    // Vector getting all names from file
    std::vector<std::string> names;
    // Read names from file
    std::ifstream file("names.txt");
    // Configurations
    int i;
    int j;
    std::string temp;
    std::string text;
    // While loop to get all the names
    while(getline(file, text))
    {
        names.push_back(text);
    }
    // Sorting algorithm which compares two names and sorts them if the value of previous name is lower
    for(int i = 1; i < 1000; i++)
    {
        j = i;
        while(j > 0)
        {
            if(names[j] < names[j-1])
            {
                temp = names[j];
                names[j] = names[j-1];
                names[j-1] = temp;
            }
            j-=1;
        }
    }
    // Insertion sort to sort first 1000 names
    for(int i = 1; i < 1000; i++)
    {
        j = i;
        while(j > 0 && names[j] < names[j-1])
        {
            temp = names[j];
            names[j] = names[j-1];
            names[j-1] = temp;
            j-=1;
        }
    }
    // Printing the 1000 names that are sorted
    for(int i = 0; i < 1000; i++)
    {
        std::cout << names[i] << std::endl;
    }
    // Closing file
    file.close();
}